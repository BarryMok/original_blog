<?php
include 'AdminName_check_session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="includes/style/font/css/font-awesome.min.css">
    <script type="text/javascript" src="includes/style/js/jquery.js"></script>
    <script type="text/javascript" src="includes/style/js/ch-ui.admin.js"></script>
    <script src="includes/layer/layer.js"></script>

    <style type="text/CSS">
        <!--
        .page a:link {
            color: #0000FF;
            text-decoration: none;
        }
        .page a:visited {
            text-decoration: none;
            color: #0000FF;
        }
        .page a:hover {
            text-decoration: none;
            color: #0000FF;
        }
        .page a:active {
            text-decoration: none;
            color: #0000FF;
        }
        .page{color:#0000FF;}
        -->
    </style>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">分类管理</a>&raquo; 分类列表
</div>
<!--面包屑导航 结束-->

<!--搜索结果页面 列表 开始-->
<form action="#" method="post">
    <div class="result_wrap">
        <!--快捷导航 开始-->
        <div class="result_content">
            <div class="short_wrap">
                <a href="#"><i class="fa fa-plus"></i>新增文章</a>
                <a href="#"><i class="fa fa-recycle"></i>批量删除</a>
                <a href="#"><i class="fa fa-refresh"></i>更新排序</a>
            </div>
        </div>
        <!--快捷导航 结束-->
    </div>

    <div class="result_wrap">
        <div class="result_content">
            <table class="list_tab">
                <tr>
                    <th class="tc" width="5%"><input type="checkbox" name=""></th>
                    <th class="tc">排序</th>
                    <th class="tc">ID</th>
                    <th>分类名称</th>
                    <th>分类描述</th>
                    <th>操作</th>
                </tr>

                <?php
                /*
                * Created on 2010-4-17
                *
                * Order by Kove Wong
                */

                include ('includes/mysql/mysql_conn.php');
                $Page_size=5;
                $sql = "select * from category";
                $result=mysqli_query($conn,$sql);
                $count = $result->num_rows;
                $page_count = ceil($count/$Page_size);

                $init=1;
                $page_len=7;
                $max_p=$page_count;
                $pages=$page_count;

                //判断当前页码
                if(empty($_GET['page'])||$_GET['page']<0){
                    $page=1;
                }else {
                    $page=$_GET['page'];
                }

                $offset=$Page_size*($page-1);
                $sql="select * from category limit $offset,$Page_size";
                $result=mysqli_query($conn,$sql);
                while ($row=mysqli_fetch_array($result)) {
                    ?>
                    <tr>
                        <td class="tc"><input type="checkbox" name="id[]" value="59"></td>
                        <td class="tc">
                            <input type="text" name="ord[]" value="<?php echo $row['cate_order']?>">
                        </td>
                        <td class="tc"><?php echo $row['id']?></td>
                        <td>
                            <a href="#"><?php echo $row['cate_name']?></a>
                        </td>
                        <td><?php echo $row['cate_title']?></td>
                        <td>

                            <a href="categoryEdit.php?id=<?php echo $row['id']?>">修改</a>

                            <a href="javascript:;" onclick="_delete(<?php echo $row['id']?>)">删除</a>
                        </td>
                    </tr>
                <?php }
                $page_len = ($page_len%2)?$page_len:$pagelen+1;//页码个数
                $pageoffset = ($page_len-1)/2;//页码个数左右偏移量


                if($pages>$page_len){
//如果当前页小于等于左偏移
                    if($page<=$pageoffset){
                        $init=1;
                        $max_p = $page_len;
                    }else{//如果当前页大于左偏移
//如果当前页码右偏移超出最大分页数
                        if($page+$pageoffset>=$pages+1){
                            $init = $pages-$page_len+1;
                        }else{
//左右偏移都存在时的计算
                            $init = $page-$pageoffset;
                            $max_p = $page+$pageoffset;
                        }
                    }
                }?>

            </table>





            <div class="page_list">
                <ul>
                    <li class="disabled"><a href="categoryList11111.php?page=<?=$page-1;?>">&laquo;</a></li>
                    <li class="active"><a href="categoryList11111.php?page=<?=$page;?>"><?=$page?></a></li>
                    <li><a href="categoryList11111.php?page=<?=$page+1;?>">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
</form>
<!--搜索结果页面 列表 结束-->
<script>

    function _delete(id) {
        layer.confirm('确认删除？', {
            btn: ['确认','取消'] //按钮
        }, function(){
            $.ajax({
                type : 'GET',
                url : 'category_delete_check.php',
                dataType : 'json',
                data : {id:id},
                success : function (data) {
                    if(data.status == 0){
                        layer.msg('删除成功!', {icon: 1, time: 1500}, //1.5秒关闭（如果不配置，默认是3秒）
                            function(){
                                //do something
                                location.href = "categoryList.php";
                            });
                    }
                },
                error :function (xhr,status) {
                    console.log(xhr);
                    console.log(status);
                }
            });
        }, function(){
            layer.msg('已取消', {
                icon: 2,
                time: 1500 //1.5秒关闭（如果不配置，默认是3秒）
            });
        });
    }
</script>


</body>