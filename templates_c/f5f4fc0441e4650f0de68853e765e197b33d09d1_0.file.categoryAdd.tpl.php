<?php
/* Smarty version 3.1.30, created on 2017-04-25 12:19:48
  from "D:\MpProject\Original_blog\templates\categoryAdd.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58ff3ee4ce5bd2_97038854',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f5f4fc0441e4650f0de68853e765e197b33d09d1' => 
    array (
      0 => 'D:\\MpProject\\Original_blog\\templates\\categoryAdd.tpl',
      1 => 1493109715,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58ff3ee4ce5bd2_97038854 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="/includes/style/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/includes/layer/layer.js"><?php echo '</script'; ?>
>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">分类管理</a> &raquo; 添加分类
</div>
<!--面包屑导航 结束-->

<div class="result_wrap">
    <form action="#" method="post">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>父级分类：</th>
                <td>

                    <select name="" class="addChange">
                        <option value="" disabled selected>==请选择==</option>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类名称：</th>
                <td>
                    <input type="text" class="lg addChange" name="cate_name">
                    <p>名称可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类标题：</th>
                <td>
                    <input type="text" class="lg addChange" name="cate_title">
                    <p>标题可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input type="button" value="提交" onclick="_login()">
                    <input type="button" class="back"  value="返回" onclick="history.go(-1);">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<?php echo '<script'; ?>
>
    function _login() {
        var fl = $('Select option:selected') .val();
        var cate_name = $('input[name = cate_name]').val();
        var cate_title = $('input[name = cate_title]').val();
        if (fl == ""){
//                alert("请选择父级分类");
            layer.msg('请选择父级分类!', {icon: 7,time:1500});
            return;
        }
        if (cate_name.length == ""){
//                alert('分类名称不能为空');
            layer.msg('分类名称不能为空!', {icon: 7,time:1500});
            return;
        }
        if(cate_title.length == ""){
//                alert('分类标题不能为空');
            layer.msg('分类标题不能为空!', {icon: 7,time:1500});
            return;
        }

        $.ajax({
                type : 'POST',//上传提交类型
                url : 'categoryAdd.php',//提交的URL路径
                dataType : 'JSON',//接受服务器返回的格式
                data : {cate_name:cate_name,cate_title:cate_title,fl:fl},//上传的数据
                success : function (data) {
                    if(data.status == 1){
                    alert(data.message);
                    return;
                    }
                if (data.status == 0){
            layer.msg('增加成功!', {icon: 1, time: 1500}, //1.5秒关闭（如果不配置，默认是3秒
                function(){
                    //do something
                    location.href = "categoryAdd.php";
                });
        }
    },
        error : function (xhr,status) {
        console.log(xhr);
        console.log(status);
        }
    })
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
