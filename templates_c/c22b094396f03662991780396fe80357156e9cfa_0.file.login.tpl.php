<?php
/* Smarty version 3.1.30, created on 2017-04-24 10:16:28
  from "D:\MpProject\Original_blog\smarty\templates\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58fdd07c922072_60152483',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c22b094396f03662991780396fe80357156e9cfa' => 
    array (
      0 => 'D:\\MpProject\\Original_blog\\smarty\\templates\\login.tpl',
      1 => 1493028951,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58fdd07c922072_60152483 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 src="/includes/style/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/includes/layer/layer.js"><?php echo '</script'; ?>
>
</head>
<body style="background:#F3F3F4;">
<div class="login_box">
    <h1>Blog</h1>
    <h2>欢迎使用博客管理平台</h2>
    <div class="form">
        <p id="_error" style="color:red"></p>
        <form action="../login.php" method="post">
            <ul>
                <li>
                    <input type="text" name="AdminName" class="text"/>
                    <span><i class="fa fa-user"></i></span>
                </li>
                <li>
                    <input type="password" name="Password" class="text"/>
                    <span><i class="fa fa-lock"></i></span>
                </li>
                <li>
                    <input type="text" class="code" name="code"/>
                    <span><i class="fa fa-check-square-o"></i></span>
                    <img src="/includes/tools/getCode.php" alt="" onclick="this.src='/includes/tools/getCode.php?'+Math.random()">
                </li>
                <li>
                    <input id="_login" type="button" value="立即登陆"/>
                </li>
            </ul>
        </form>
        <p><a href="#">返回首页</a> &copy; 2016 Powered by <a href="http://www.chenhua.club" target="_blank">http://www.chenhua.club</a></p>
    </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function () {
        $('#_login').click(function () {
            var AdminName = $('input[name = AdminName]').val();
            var Password = $('input[name = Password]').val();
            var Code = $('input[name = code]').val();
            var action_url = $('form').attr('action');

            if (AdminName.length == ""){
                layer.tips('用户名不能为空!', '.text', {
                    tips: [2, '#666']
                });
                return;
            }
            if(Password.length == ""){
                layer.tips('密码不能为空!', '.pwd', {
                    tips: [2, '#666']
                });
                return;
            }
            if(Code.length == ""){
//            layer.msg('验证码不能为空!', {icon: 7});
                layer.tips('验证码不能为空!', '.code', {
                    tips: [4, '#666']
                });
                return;
            }
            $.ajax({
                type:'POST',
                url:action_url,
                dataType:'JSON',
                data:$('from').serialize(),
                success : function (data) {
                    if(data.status == 1){
                        alert(data.message);
                        return;
                    }
                    if(data.status == 3){
//                    layer.msg('验证码错误!', {icon: 2});
                        layer.tips('验证码错误!', '.code', {
                            tips: [4, '#FF5722']
                        });
//                    $('img').attr('src','/include/tools/getCode.php');
                        return false;
                    }
                    if(data.status == 2){
//                        $('#_error').html('用户名或密码不正确!');
                        layer.msg('用户名或密码不正确!', {icon: 2});
                        return false;
                    }
                    if (data.status == 0){
                        layer.msg('正在登录...',{icon:16,shade: 0.8,time:1500}, function(){
                            //do something
                            location.href = "../index.php";
                        });
                    }
                },
                error : function (xhr,status) {
                console.log(xhr);
                console.log(status);
                }
            })
        })
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
