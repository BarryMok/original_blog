<?php
/* Smarty version 3.1.30, created on 2017-04-26 05:39:54
  from "D:\MpProject\Original_blog\templates\categoryList.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_590032aa79eb97_52226159',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c7310cedfe8588a1cef5ee2b863d70f08b9b2a2' => 
    array (
      0 => 'D:\\MpProject\\Original_blog\\templates\\categoryList.tpl',
      1 => 1493185082,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_590032aa79eb97_52226159 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="/includes/style/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/includes/style/js/ch-ui.admin.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/includes/layer/layer.js"><?php echo '</script'; ?>
>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">分类管理</a>&raquo; 分类列表
</div>
<!--面包屑导航 结束-->

<!--结果页快捷搜索框 开始-->
<div class="search_wrap">
    <form action="../categoryList.php" method="post">
        <table class="search_tab">
            <tr>
                <th width="120">选择分类:</th>
                <td>
                    <select onchange="javascript:location.href=this.value;">
                        <option value="">全部</option>
                        <option value="http://www.baidu.com">百度</option>
                        <option value="http://www.sina.com">新浪</option>
                    </select>
                </td>
                <th width="70">关键字:</th>
                <td><input type="text" name="keywords" value="" placeholder="关键字"></td>
                <td><input type="submit" name="sub" value="查询"></td>
            </tr>
        </table>
    </form>
</div>
<!--结果页快捷搜索框 结束-->

<!--搜索结果页面 列表 开始-->
<form action="#" method="post">


    <div class="result_wrap">
        <div class="result_content">
            <table class="list_tab">
                <tr>
                    <th class="tc" width="5%"><input type="checkbox" name=""></th>
                    <th class="tc">排序</th>
                    <th class="tc">ID</th>
                    <th>分类名称</th>
                    <th>分类描述</th>
                    <th>操作</th>
                </tr>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                <tr>
                <td class="tc"><input type="checkbox" name="id[]" value="59"></td>
                <td class="tc">
                <input type="text" name="ord[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['order'];?>
">
                </td>
                <td class="tc"><?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
</td>
                <td>
                <a href="#"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</a>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</td>
                <td>
                <a href="../categoryEdit.php?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">修改</a>
                <a href="javascript:;" onclick="_delete(<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
)">删除</a>
                </td>
                </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </table>

            <div class="page_list">
                <ul>
                    <li class="disabled"><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
</form>
<!--搜索结果页面 列表 结束-->
<?php echo '<script'; ?>
>
    function _delete(id) {
        layer.confirm('确认删除？', {
            btn: ['确认','取消'] //按钮
        }, function(){
            $.ajax({
                type : 'GET',
                url : 'categoryList_delete_check.php',
                dataType : 'json',
                data : {id:id},
                success : function (data) {
                    if(data.status == 0){
                        layer.msg('删除成功!', {icon: 1, time: 1500}, //1.5秒关闭（如果不配置，默认是3秒）
                            function(){
                                //do something
                                location.href = "../categoryList.php";
                            });
                    }
                },
                error :function (xhr,status) {
                    console.log(xhr);
                    console.log(status);
                }
            });
        }, function(){
            layer.msg('已取消', {
                icon: 2,
                time: 1500 //1.5秒关闭（如果不配置，默认是3秒）
            });
        });
    }
<?php echo '</script'; ?>
>


</body>
</html><?php }
}
