<?php
/* Smarty version 3.1.30, created on 2017-04-25 06:14:46
  from "D:\MpProject\Original_blog\templates\articleEdit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58fee956ceea28_09561015',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a7609db1add31e1728a26e0415346fb208e61a4' => 
    array (
      0 => 'D:\\MpProject\\Original_blog\\templates\\articleEdit.tpl',
      1 => 1493100877,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58fee956ceea28_09561015 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="/includes/style/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/includes/layer/layer.js"><?php echo '</script'; ?>
>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">文章管理</a>&raquo; <a href="#">文章列表</a> &raquo; 修改文章
</div>

<div class="result_wrap">
    <form action="#" method="post">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>分类：</th>
                <td>
                    <select name="">
                        <option value="" disabled>==请选择==</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['v']->value['id'] == $_smarty_tpl->tpl_vars['data1']->value['cate_id']) {?>
                        <option class="lg" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</option>
                        <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</option>
                        <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>文章标题：</th>
                <td>
                    <input type="text" class="lg" name="art_title" value="<?php echo $_smarty_tpl->tpl_vars['data1']->value['art_title'];?>
">
                    <p>标题可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th>作者：</th>
                <td>
                    <input type="text" name="art_editor" value="<?php echo $_smarty_tpl->tpl_vars['data1']->value['art_editor'];?>
">
                    <span><i class="fa fa-exclamation-circle yellow"></i>这里是默认长度</span>
                </td>
            </tr>

            <tr>
                <th>图片路径：</th>
                <td>
                    <input type="text" size="50" name="art_thumb" style="margin-top: 22px;" value="<?php echo $_smarty_tpl->tpl_vars['data1']->value['art_thumb'];?>
" disabled>
                    <input id="file_upload" name="file_upload" type="file" multiple="true">
                    <?php echo '<script'; ?>
 src="/includes/uploadify/jquery.uploadify.min.js" type="text/javascript"><?php echo '</script'; ?>
>
                    <link rel="stylesheet" type="text/css" href="/includes/uploadify/uploadify.css">
                    <?php echo '<script'; ?>
 type="text/javascript">
                        <?php $_smarty_tpl->_assignInScope('timestamp', time());
?>
                        $(function() {
                            $('#file_upload').uploadify({
                                'buttonText' : '图片上传',
                                'formData'     : {
                                    'timestamp' : '<?php echo $_smarty_tpl->tpl_vars['timestamp']->value;?>
',
                                    'token'     : '<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
'
                                },
                                'swf'      : "/includes/uploadify/uploadify.swf",
                                'uploader' : "/includes/uploadify/uploadify.php",
                                //回调
                                'onUploadSuccess' : function(file, data, response) {
                                    //返回PHP图片返回的路径
                                    $('input[name=art_thumb]').val(data);
                                    //给图片加一个src值
                                    $('#art_thumb_img').attr('src',''+data);
//                                    alert(data);
                                }
                            });
                        });
                    <?php echo '</script'; ?>
>
                    <style>
                        .uploadify{display:inline-block;}
                        .uploadify-button{border:none; border-radius:5px; margin-top:8px;}
                        table.add_tab tr td span.uploadify-button-text{color: #FFF; margin:0;}
                    </style>
                </td>
            </tr>

            <tr>
                <th>缩略图:</th>
                <td>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['data1']->value['art_thumb'];?>
" alt="" id="art_thumb_img" style="max-width: 350px; max-height:100px;">
                </td>
            <tr>

            <tr>
                <th>文章内容：</th>
                <td>
                    <?php echo '<script'; ?>
 type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> <?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 id="editor" name="art_content" class="lg" type="text/plain" style="width:1100px;height:400px;" ><?php echo $_smarty_tpl->tpl_vars['data1']->value['art_content'];
echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 type="text/javascript">
                        var ue = UE.getEditor('editor');
                    <?php echo '</script'; ?>
>
                    <style>
                        .edui-default{line-height: 28px;};
                        div.edui-combox-body,div.edui-button-body,div.edui-splitbutton-body;
                        {overflow: hidden; height:20px;}
                        div.edui-box{overflow: hidden; height:22px;}
                    </style>
                </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <input id="_change" type="button" value="提交"  onclick="_update(<?php echo $_smarty_tpl->tpl_vars['data1']->value['id'];?>
)">
                    <input type="button" class="back" onclick="history.go(-1)" value="返回">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>

<?php echo '<script'; ?>
>

    function _update(id) {
        var fl = $('Select option:selected') .val();
        var art_title = $('input[name = art_title]').val();
        var art_thumb = $('input[name = art_thumb]').val();
        var art_editor = $('input[name = art_editor]').val();
        var art_content = ue.getContent();
        if (fl == ""){
            layer.msg('请选择分类!', {icon: 7,time:1500});
            return;
        }
        if (art_title == ""){
            layer.msg('文章标题不能为空!', {icon: 7,time:1500});
            return;
        }
        if(art_thumb == ""){
            layer.msg('缩略图不能为空!', {icon: 7,time:1500});
            return;
        }

        if(art_editor == ""){
            layer.msg('作者不能为空!', {icon: 7,time:1500});
            return;
        }

        if(art_content == ""){
            layer.msg('文章内容不能为空!', {icon: 7,time:1500});
            return;
        }

        $.ajax({
                type : 'POST',//上传提交类型
                url : 'articleEdit.php',//提交的URL路径
                dataType : 'JSON',//接受服务器返回的格式
                data : {id:id,art_title:art_title,art_thumb:art_thumb,art_editor:art_editor,fl:fl,art_content:art_content},//上传的数据
                success : function (data) {
                    if(data.status == 1){
                    alert(data.message);
                    return;
                    }
                if (data.status == 0){
            layer.msg('修改成功!', {icon: 1, time: 1500}, //1.5秒关闭（如果不配置，默认是3秒
                function(){
                    //do something
                    location.href = "articleList.php";
                });
        }
    },
        error : function (xhr,status) {
        console.log(xhr);
        console.log(status);
        }
    })
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
