<?php
/* Smarty version 3.1.30, created on 2017-04-25 02:09:41
  from "D:\MpProject\Original_blog\templates\categoryEdit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58feafe5c2b019_76008808',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '14e377ab89f3ccc125a31cafdaf3c1df77eda258' => 
    array (
      0 => 'D:\\MpProject\\Original_blog\\templates\\categoryEdit.tpl',
      1 => 1493086177,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58feafe5c2b019_76008808 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="/includes/style/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/includes/layer/layer.js"><?php echo '</script'; ?>
>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">分类管理</a>&raquo; <a href="#">分类列表</a> &raquo; 修改分类
</div>

<div class="result_wrap">
    <form action="#" method="post">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>父级分类：</th>
                <td>

                    <select id="cate_pid">
                        <option value="" disabled>==请选择==</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>

                        <?php if ($_smarty_tpl->tpl_vars['v']->value['id'] == $_smarty_tpl->tpl_vars['data1']->value['cate_pid']) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['v']->value['cate_name'];?>
</option>

                        <?php } elseif ($_smarty_tpl->tpl_vars['data1']->value['cate_pid'] == 0) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" disabled selected>已经是父级分类,无法修改!</option>

                        <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['cate_name'];?>
</option>

                        <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类名称：</th>
                <td>
                    <input type="text" class="lg" name="cate_name" value="<?php echo $_smarty_tpl->tpl_vars['data1']->value['cate_name'];?>
">
                    <p>名称可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类标题：</th>
                <td>
                    <input type="text" class="lg" name="cate_title" value="<?php echo $_smarty_tpl->tpl_vars['data1']->value['cate_title'];?>
">
                    <p>标题可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input id="_change" type="button" class="back" disabled="disabled" value="提交" onclick="_update(<?php echo $_smarty_tpl->tpl_vars['data1']->value['id'];?>
)">
                    <input type="button" class="back" onclick="history.go(-1)" value="返回">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>

<?php echo '<script'; ?>
>
    $('.lg').change(function () {
        var _change = $('#_change');
        _change.removeClass('back');
        _change.removeAttr("disabled");
    });
    function _update(id) {
        var cate_pid = $('select option:selected').val();
        var cate_name = $('input[name = cate_name]').val();
        var cate_title = $('input[name = cate_title]').val();
        if(cate_pid == ''){
            layer.msg('父级分类不能为空!', {icon: 0, time: 1500});
            return;
        }
        if (cate_name == ''){
            layer.msg('分类名称不能为空!', {icon: 0, time: 1500});
            return;
        }
        if (cate_title == ''){
            layer.msg('分类标题不能为空!', {icon: 0, time: 1500});
            return;
        }
        $.ajax({
            type: 'POST',
            url: 'categoryEdit.php',
            dataType: 'JSON',
            data: {id: id, cate_pid: cate_pid, cate_name: cate_name, cate_title: cate_title},
            success: function (data) {
                if (data.status == 1) {
                    layer.msg(data.message, {icon: 0, time: 1500}); //1.5秒关闭（如果不配置，默认是3秒）
                    return;
                }
                if(data.status == 2){
                    layer.msg(data.message, {icon: 2, time: 1500}); //1.5秒关闭（如果不配置，默认是3秒）
                    return;
                }
                layer.msg(data.message, {
                    icon: 6,
                    time: 1500
                }, function () {
                    location.href = "../categoryList.php";
                })
            },
            error: function (xhr, status) {
                console.log(xhr);
                console.log(status);
            }
        })
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
