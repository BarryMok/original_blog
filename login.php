<?php
include_once 'init.php';

if(isset($_SESSION['AdminName']) && !empty($_SESSION['AdminName'])){
    $smarty->display('index.tpl');
}

$AdminName = $_POST['AdminName']?strip_tags($_POST['AdminName']) : '';
$Password = $_POST['Password']?strip_tags($_POST['Password']) : '';
$code = $_POST['code']?strip_tags($_POST['code']) : '';

/*校验*/
if ($AdminName == ''){
    $data = array('status'=>1,'message'=>'用户名不能为空!');
    die(json_encode($data));
}
if ($Password == ''){
    $data = array('status'=>1,'message'=>'密码不能为空!');
    die(json_encode($data));
}
if($code == ''){
    $data = array('status'=>1,'message'=>'验证码不能为空!');
    die(json_encode($data));
}

/*验证码校验 先将用户提交的验证码全部转换为大写再校验*/
if($_SESSION['code'] != strtoupper($code)){
//        $data = array('status'=>3,'message'=>'验证码错误!');
    $data = array('status'=>3,'message'=>'验证码错误!');
    die(json_encode($data));
}


$sql = "SELECT * from admin WHERE AdminName = '$AdminName'";
$result = mysqli_query($conn,$sql);

/*查询不到数据则返回*/
$v = $result->fetch_row();
if($v == ""){
    $data = array('status'=>2,'message'=>'');
    die(json_encode($data));
}
if(md5($Password) != $v[2]){
    $data = array('status'=>2,'message'=>'');
    die(json_encode($data));
}

//登陆成功
//    header('location:message.html');
$_SESSION['AdminName'] = $AdminName;
$data = array('status' => 0,'message' => "");
die(json_encode($data));

