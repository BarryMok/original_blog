<?php
include 'AdminName_check_session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">

</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a>
</div>
<!--面包屑导航 结束-->


<div class="result_wrap">
    <div class="result_title">
        <h3>系统基本信息</h3>
    </div>
    <div class="result_content">
        <ul>
            <li>
                <label>操作系统</label><span><?php echo PHP_OS ?></span>
            </li>
            <li>
                <label>运行环境</label><span><?php echo $_SERVER['SERVER_SOFTWARE'] ?></span>
            </li>
            <li>
                <label>PHP运行方式</label><span><?php echo php_sapi_name() ?></span>
            </li>
            <li>
                <label>-版本</label><span>v-0.1</span>
            </li>
            <li>
                <label>上传附件限制</label><span><?php echo get_cfg_var ("upload_max_filesize")?get_cfg_var ("upload_max_filesize"):"不允许上传附件"; ?></span>
            </li>
            <li>
                <label>北京时间</label><span><?php date_default_timezone_set('PRC'); echo date('Y年m月d日 H时i分s秒');?></span>
            </li>
            <li>
                <label>服务器域名/IP</label><span><?php echo  $_SERVER["HTTP_HOST"] ?>/<?php echo GetHostByName($_SERVER['SERVER_NAME'])?></span>
            </li>
            <li>
                <label>Host</label><span><?php echo $_SERVER['SERVER_ADDR'] ?></span>
            </li>
        </ul>
    </div>
</div>


<div class="result_wrap">
    <div class="result_title">
        <h3>使用帮助</h3>
    </div>
    <div class="result_content">
        <ul>
            <li>
                <label>官方交流网站：</label><span><a href="http://www.panda.tv/632096" target="_blank">http://www.panda.tv/632096</a></span>
            </li>
            <li>
                <label>官方交流QQ群：</label><span>271162523</span>
            </li>
        </ul>
    </div>
</div>
<!--结果集列表组件 结束-->

</body>