<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <script type="text/javascript" src="/includes/style/js/jquery.js"></script>
    <script type="text/javascript" src="/includes/style/js/ch-ui.admin.js"></script>
    <script src="/includes/layer/layer.js"></script>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">文章管理</a> &raquo; 添加文章
</div>
<!--面包屑导航 结束-->

<div class="result_wrap">
    <form action="#" method="post">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>分类：</th>
                <td>
                    <select name="">
                        <option value="" disabled selected>==请选择==</option>
                        <{foreach from = $list item='v'}>
                        <option value="<{$v['id']}>"><{$v['name']}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>文章标题：</th>
                <td>
                    <input type="text" class="lg" name="art_title">
                    <p>标题可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th>作者：</th>
                <td>
                    <input type="text" name="art_editor">
                    <span><i class="fa fa-exclamation-circle yellow"></i>这里是默认长度</span>
                </td>
            </tr>

            <tr>
                <th>图片路径：</th>
                <td>
                    <input type="text" size="50" name="art_thumb" style="margin-top: 22px;"  disabled  >
                    <input id="file_upload" name="file_upload" type="file" multiple="true">
                    <script src="/includes/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
                    <link rel="stylesheet" type="text/css" href="/includes/uploadify/uploadify.css">
                    <script type="text/javascript">

                        $(function() {
                            $('#file_upload').uploadify({
                                'buttonText' : '图片上传',
                                'formData'     : {
                                    'timestamp' : '<{$timestamp}>',
                                    'token'     : '<{$token}>'
                                },
                                'swf'      : "/includes/uploadify/uploadify.swf",
                                'uploader' : "/includes/uploadify/uploadify.php",
                                //回调
                                'onUploadSuccess' : function(file, data, response) {
                                    //返回PHP图片返回的路径
                                    $('input[name=art_thumb]').val(data);
                                    //给图片加一个src值
                                    $('#art_thumb_img').attr('src',''+data);
//                                    alert(data);
                                }
                            });
                        });
                    </script>
                    <style>
                        .uploadify{display:inline-block;}
                        .uploadify-button{border:none; border-radius:5px; margin-top:8px;}
                        table.add_tab tr td span.uploadify-button-text{color: #FFF; margin:0;}
                    </style>
                </td>
            </tr>

            <tr>
                <th>缩略图：</th>
                <td>
                    <img src="" alt="" id="art_thumb_img" style="max-width: 350px; max-height:100px;">
                </td>
            <tr>

            <tr>
                <th>文章内容：</th>
                <td>
                    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
                    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
                    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
                    <script id="editor" name="art_content" type="text/plain" style="width:1100px;height:400px;"></script>
                    <script type="text/javascript">
                        var ue = UE.getEditor('editor');

                    </script>
                    <style>
                        .edui-default{line-height: 28px;}
                        div.edui-combox-body,div.edui-button-body,div.edui-splitbutton-body
                        {overflow: hidden; height:20px;}
                        div.edui-box{overflow: hidden; height:22px;}
                    </style>
                </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <input type="button" value="提交" onclick="_login()">
                    <input type="button" class="back" onclick="history.go(-1)" value="返回">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    function _login() {
        var fl = $('Select option:selected') .val();
        var art_title = $('input[name = art_title]').val();
        var art_thumb = $('input[name = art_thumb]').val();
        var art_editor = $('input[name = art_editor]').val();
        var art_content = ue.getContent();
        if (fl == ""){
            layer.msg('请选择分类!', {icon: 7,time:1500});
            return;
        }
        if (art_title == ""){
            layer.msg('文章标题不能为空!', {icon: 7,time:1500});
            return;
        }
        if(art_thumb == ""){
            layer.msg('缩略图不能为空!', {icon: 7,time:1500});
            return;
        }

        if(art_editor == ""){
            layer.msg('作者不能为空!', {icon: 7,time:1500});
            return;
        }

        if(art_content == ""){
            layer.msg('文章内容不能为空!', {icon: 7,time:1500});
            return;
        }
        if(art_thumb == "请上传正确的格式图片!"){
            layer.msg('请上传正确的格式图片!', {icon: 7,time:1500});
            return;
        }
        $.ajax({
                type : 'POST',//上传提交类型
                url : 'articleAdd.php',//提交的URL路径
                dataType : 'JSON',//接受服务器返回的格式
                data : {art_title:art_title,art_thumb:art_thumb,art_editor:art_editor,fl:fl,art_content:art_content},//上传的数据
                success : function (data) {
                    if(data.status == 1){
                    alert(data.message);
                    return;
                    }
                if (data.status == 0){
            layer.msg('增加成功!', {icon: 1, time: 1500}, //1.5秒关闭（如果不配置，默认是3秒
                function(){
                    //do something
                    location.href = "articleAdd.php";
                });
        }
    },
        error : function (xhr,status) {
        console.log(xhr);
        console.log(status);
        }
    })
    }
</script>
</body>
</html>