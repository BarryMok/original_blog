<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <script type="text/javascript" src="/includes/style/js/jquery.js"></script>
    <script src="/includes/layer/layer.js"></script>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; <a href="#">分类管理</a>&raquo; <a href="#">分类列表</a> &raquo; 修改分类
</div>

<div class="result_wrap">
    <form action="#" method="post">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>父级分类：</th>
                <td>

                    <select id="cate_pid">
                        <option value="" disabled>==请选择==</option>
                        <{foreach from = $list item='v'}>

                        <{if $v['id'] eq $data1['cate_pid']}>
                        <option value="<{$v['id']}>" selected><{$v['cate_name']}></option>

                        <{elseif $data1['cate_pid'] eq 0}>
                        <option value="<{$v['id']}>" disabled selected>已经是父级分类,无法修改!</option>

                        <{else}>
                        <option value="<{$v['id']}>"><{$v['cate_name']}></option>

                        <{/if}>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类名称：</th>
                <td>
                    <input type="text" class="lg" name="cate_name" value="<{$data1['cate_name']}>">
                    <p>名称可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>分类标题：</th>
                <td>
                    <input type="text" class="lg" name="cate_title" value="<{$data1['cate_title']}>">
                    <p>标题可以写30个字</p>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input id="_change" type="button" class="back" disabled="disabled" value="提交" onclick="_update(<{$data1['id']}>)">
                    <input type="button" class="back" onclick="history.go(-1)" value="返回">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>

<script>
    $('.lg').change(function () {
        var _change = $('#_change');
        _change.removeClass('back');
        _change.removeAttr("disabled");
    });
    function _update(id) {
        var cate_pid = $('select option:selected').val();
        var cate_name = $('input[name = cate_name]').val();
        var cate_title = $('input[name = cate_title]').val();
        if(cate_pid == ''){
            layer.msg('父级分类不能为空!', {icon: 0, time: 1500});
            return;
        }
        if (cate_name == ''){
            layer.msg('分类名称不能为空!', {icon: 0, time: 1500});
            return;
        }
        if (cate_title == ''){
            layer.msg('分类标题不能为空!', {icon: 0, time: 1500});
            return;
        }
        $.ajax({
            type: 'POST',
            url: 'categoryEdit.php',
            dataType: 'JSON',
            data: {id: id, cate_pid: cate_pid, cate_name: cate_name, cate_title: cate_title},
            success: function (data) {
                if (data.status == 1) {
                    layer.msg(data.message, {icon: 0, time: 1500}); //1.5秒关闭（如果不配置，默认是3秒）
                    return;
                }
                if(data.status == 2){
                    layer.msg(data.message, {icon: 2, time: 1500}); //1.5秒关闭（如果不配置，默认是3秒）
                    return;
                }
                layer.msg(data.message, {
                    icon: 6,
                    time: 1500
                }, function () {
                    location.href = "../categoryList.php";
                })
            },
            error: function (xhr, status) {
                console.log(xhr);
                console.log(status);
            }
        })
    }
</script>
</body>
</html>