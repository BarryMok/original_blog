<?php
include 'AdminName_check_session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <script type="text/javascript" src="/includes/style/js/jquery.js"></script>
    <script type="text/javascript" src="/includes/style/js/ch-ui.admin.js"></script>
    <script src="/includes/layer/layer.js"></script>
</head>
<body>
<!--面包屑导航 开始-->
<div class="crumb_warp">
    <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
    <i class="fa fa-home"></i> <a href="#">首页</a> &raquo; 修改密码
</div>
<!--面包屑导航 结束-->

<!--结果集标题与导航组件 开始-->
<div class="result_wrap">
    <div class="result_title">
        <h3>修改密码</h3>
    </div>
</div>
<!--结果集标题与导航组件 结束-->

<div class="result_wrap">
    <form method="post" onsubmit="return changePass()">
        <input type="hidden" name="_token" value="">
        <table class="add_tab">
            <tbody>
            <tr>
                <th width="120"><i class="require">*</i>原密码：</th>
                <td>
                    <input type="password" name="password_o"> </i>请输入原密码</span>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>新密码：</th>
                <td>
                    <input type="password" name="password"> </i>新密码6-20位</span>
                </td>
            </tr>
            <tr>
                <th><i class="require">*</i>确认密码：</th>
                <td>
                    <input type="password" name="password_c"> </i>再次输入密码</span>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input type="button" value="提交" onclick="_login()">
                    <input type="button" class="back" onclick="history.go(-1)" value="返回">
                    <span id="_error" style="color: red"></span>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    function _login() {
        var password_o = $('input[name = password_o]').val();
        var password = $('input[name = password]').val();
        var password_c = $('input[name = password_c]').val();
        if (password_o.length == ""){
//                alert("请输入原密码!");
            layer.msg('请输入原密码!', {icon: 7});
            return;
        }
        if(password.length == ""){
//                alert("请输入新密码!");
            layer.msg('请输入新密码!', {icon: 7});
            return;
        }
        if(password_c.length == ""){
//                alert("请输入确认密码!");
            layer.msg('请输入确认密码!', {icon: 7});
            return;
        }
        if(password.length<6 || password.length>20){
//                alert("新密码必须在6-20位之间!");
            layer.msg('新密码必须在6-20位之间!', {icon: 7});
            return;
        }
        if (password == password_o){
//                alert("原密码与新密码不能一致!");
            layer.msg('原密码与新密码不能一致!', {icon: 2});
            return;
        }
        if(password != password_c){
//                alert("确认密码不一致,请确认后提交!");
            layer.msg('确认密码不一致,请确认后提交!', {icon: 2});
            return;
        }

        $.ajax({
            type : 'POST',//上传提交类型
            url : 'pass_check.php',//提交的URL路径
            data : {password_o:password_o,password:password,password_c:password_c},//上传的数据
            success : function (data) {//成功返回时进入的方法
                if(data.status == 2){
                    layer.msg('原密码输入有误!', {icon: 2,time:1500});
                    return;
                }
                if (data.status == 0){
                    layer.alert('修改成功,请重新登录!', function(index){
                        //do something
                        window.top.location = "login.php";
                        layer.close(index);
                    });

                }
            },
            error : function (xhr,status) {//失败时进入此方法
                console.log(xhr);
                console.log(status);
            }
        })
    }
</script>
</body>