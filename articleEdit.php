<?php

include_once 'init.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $id = $_POST['id'];
    $art_title = $_POST['art_title'] ? strip_tags($_POST['art_title']) : '';
    $art_thumb = $_POST['art_thumb'] ? strip_tags($_POST['art_thumb']) : '';
    $art_editor = $_POST['art_editor'] ? strip_tags($_POST['art_editor']) : '';
    $art_content = $_POST['art_content'];
    $fl = $_POST['fl'] ? strip_tags($_POST['fl']) : '';
    $time = date("Y-m-d H:i:s");
    /*校验*/
    if ($fl == ''){
        $data = array('status'=>1,'message'=>'请选择分类!');
        die(json_encode($data));
    }
    if ($art_title == ''){
        $data = array('status'=>1,'message'=>'文章标题不能为空!');
        die(json_encode($data));
    }
    if($art_editor == ''){
        $data = array('status'=>1,'message'=>'作者不能为空!');
        die(json_encode($data));
    }
    if($art_thumb == ''){
        $data = array('status'=>1,'message'=>'缩略图不能为空!');
        die(json_encode($data));
    }
    if($art_content == ''){
        $data = array('status'=>1,'message'=>'文章内容不能为空!');
        die(json_encode($data));
    }

    include "includes/mysql/mysql_conn.php";

    $sql = "UPDATE article SET art_title='$art_title',art_thumb='$art_thumb',art_content='$art_content',art_editor='$art_editor',cate_id='$fl' WHERE id = '{$id}'";
    $result = mysqli_query($conn,$sql);
    $data = array('status'=>0,'message'=>'修改成功!');
    die(json_encode($data));
}else{
    $id = $_GET['id'];
    $sql = "SELECT * FROM article WHERE id = '{$id}'";
    $result1 = @mysqli_query($conn, $sql);
    $data1 = $result1->fetch_assoc();//获取到结果集


    $sql = "SELECT * FROM category";
    $res = @mysqli_query($conn,$sql);
    while ($result = @mysqli_fetch_assoc($res)){
        $list[] = array(
            'name' => $result['cate_name'],
            'id' => $result['id']
        );
    }

    $timestamp = time();
    $token = md5('unique_salt' . $timestamp);

    $smarty->assign('list',$list);
    $smarty->assign('timestamp',$timestamp);
    $smarty->assign('token',$token);
    $smarty->assign('data1',$data1);
    $smarty->display('articleEdit.tpl');



}
?>