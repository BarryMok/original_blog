<?php
session_start();

$password_o = md5($_POST['password_o']? strip_tags($_POST['password_o'] ) : '');
$password = md5($_POST['password']? strip_tags($_POST['password'] ) : '');
$password_c = md5($_POST['password_c']? strip_tags($_POST['password_c'] ) : '');

if ($password_o == ''){
    $data = array('status'=>1,'message'=>'原密码不能为空!');
    die(json_encode($data));
}
if($password == ''){
    $data = array('status'=>1,'message'=>'请输入新密码!');
    die(json_encode($data));
}
if($password_c == ''){
    $data = array('status'=>1,'message'=>'请输入确认密码!');
    die(json_encode($data));
}
if($password_o == $password){
    $data = array('status'=>1,'message'=>'原密码与新密码不能一致!');
    die(json_encode($data));
}
if($password_c != $password){
    $data = array('status'=>1,'message'=>'确认密码不一致,请确认后提交!');
    die(json_encode($data));
}


include "includes/mysql/mysql_conn.php";


$AdminName = $_SESSION['AdminName'];
$sql = "SELECT Password FROM admin WHERE AdminName = '{$_SESSION['AdminName']}'";
$result = mysqli_query($conn,$sql);
$p = $result->fetch_row();
if ($p[0] != $password_o){
    $data = array('status'=>2,'message'=>'原密码输入有误!');
    die(json_encode($data));
}

$sql = "UPDATE admin SET Password = '$password' WHERE AdminName = '$AdminName'";
$result = mysqli_query($conn,$sql);

$data = array('status' => 0,'message' => "修改成功!请重新登录!");
die(json_encode($data));