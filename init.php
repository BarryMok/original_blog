
<?php
//先引入smarty核心类
include_once('libs/Smarty.class.php');

//创建smarty对象
$smarty =  new Smarty;

//配置smarty
//1.模板文件目录
$smarty->template_dir = 'templates/';

//2.配置编译文件目录
$smarty->compile_dir = 'templates_c/';

//3.配置配置文件目录
$smarty->config_dir = 'config/';

//4.配置缓存文件目录
$smarty->cache_dir = 'cache/';

//修改左右定界符
$smarty->left_delimiter = '<{';
$smarty->right_delimiter = '}>';

session_start();


include_once('includes/mysql/mysql_conn.php');