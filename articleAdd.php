<?php
include_once ('init.php');

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $art_title = $_POST['art_title'] ? strip_tags($_POST['art_title']) : '';
    $art_thumb = $_POST['art_thumb'] ? strip_tags($_POST['art_thumb']) : '';
    $art_editor = $_POST['art_editor'] ? strip_tags($_POST['art_editor']) : '';
    $art_content = $_POST['art_content'];
    $fl = $_POST['fl'] ? strip_tags($_POST['fl']) : '';
    $time = date("Y-m-d H:i:s");
    /*校验*/
    if ($fl == ''){
        $data = array('status'=>1,'message'=>'请选择分类!');
        die(json_encode($data));
    }
    if ($art_title == ''){
        $data = array('status'=>1,'message'=>'文章标题不能为空!');
        die(json_encode($data));
    }
    if($art_editor == ''){
        $data = array('status'=>1,'message'=>'作者不能为空!');
        die(json_encode($data));
    }
    if($art_thumb == ''){
        $data = array('status'=>1,'message'=>'缩略图不能为空!');
        die(json_encode($data));
    }
    if($art_content == ''){
        $data = array('status'=>1,'message'=>'文章内容不能为空!');
        die(json_encode($data));
    }


    $sql = "INSERT INTO article (art_title,art_thumb,art_content,art_time,art_editor,cate_id) VALUES ('$art_title','$art_thumb','$art_content','$time','$art_editor','$fl')";
    $result = mysqli_query($conn,$sql);
    $data = array('status'=>0,'message'=>'添加成功!');
    die(json_encode($data));
}else{
    $sql = "SELECT * FROM category";
    $res = @mysqli_query($conn,$sql);
    while ($result = @mysqli_fetch_assoc($res)){
        $list[] = array(
            'name' => $result['cate_name'],
            'id' => $result['id']
        );
    }

    $timestamp = time();
    $token = md5('unique_salt' . $timestamp);

    $smarty->assign('list',$list);
    $smarty->assign('timestamp',$timestamp);
    $smarty->assign('token',$token);
    $smarty->display('articleAdd.tpl');


}

?>

