<?php
include 'AdminName_check_session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/includes/style/css/ch-ui.admin.css">
    <link rel="stylesheet" href="/includes/style/font/css/font-awesome.min.css">
    <script type="text/javascript" src="/includes/style/js/jquery.js"></script>
    <script type="text/javascript" src="/includes/style/js/ch-ui.admin.js"></script>
    <script src="/includes/layer/layer.js"></script>

</head>
<body>
<!--头部 开始-->
<div class="top_box">
    <div class="top_left">
        <div class="logo">后台管理模板</div>
        <ul>
            <li><a href="#" class="active">首页</a></li>
            <li><a href="#">管理页</a></li>
        </ul>
    </div>
    <div class="top_right">
        <ul>
            <li>管理员：<?php echo $_SESSION['AdminName']?></li>
            <li><a href="pass.php" target="main">修改密码</a></li>
            <li><a href="#" onclick="_out()">退出</a></li>
        </ul>
    </div>
</div>
<!--头部 结束-->

<!--左侧导航 开始-->
<div class="menu_box">
    <ul>
        <li>
            <h3><i class="fa fa-fw fa-clipboard"></i>文章分类管理</h3>
            <ul class="sub_menu">
                <li><a href="categoryAdd.php" target="main"><i class="fa fa-fw fa-plus-square"></i>添加分类</a></li>
                <li><a href="categoryList.php" target="main"><i class="fa fa-fw fa-list-ul"></i>分类列表</a></li>
                <li><a href="articleAdd.php" target="main"><i class="fa fa-fw fa-list-alt"></i>添加文章</a></li>
                <li><a href="articleList.php" target="main"><i class="fa fa-fw fa-image"></i>文章列表</a></li>
            </ul>
        </li>
        <li>
            <h3><i class="fa fa-fw fa-cog"></i>系统设置</h3>
            <ul class="sub_menu">
                <li><a href="#" target="main"><i class="fa fa-fw fa-cubes"></i>网站配置</a></li>
                <li><a href="#" target="main"><i class="fa fa-fw fa-database"></i>备份还原</a></li>
            </ul>
        </li>
        <li>
            <h3><i class="fa fa-fw fa-thumb-tack"></i>工具导航</h3>
            <ul class="sub_menu">
                <li><a href="http://www.yeahzan.com/fa/facss.html" target="main"><i class="fa fa-fw fa-font"></i>图标调用</a></li>
                <li><a href="http://hemin.cn/jq/cheatsheet.html" target="main"><i class="fa fa-fw fa-chain"></i>Jquery手册</a></li>
                <li><a href="http://tool.c7sky.com/webcolor/" target="main"><i class="fa fa-fw fa-tachometer"></i>配色板</a></li>
                <li><a href="element.php" target="main"><i class="fa fa-fw fa-tags"></i>其他组件</a></li>
            </ul>
        </li>
    </ul>
</div>
<!--左侧导航 结束-->

<!--主体部分 开始-->
<div class="main_box">
    <iframe src="info.php" frameborder="0" width="100%" height="100%" name="main"></iframe>
</div>
<!--主体部分 结束-->

<!--底部 开始-->
<div class="bottom_box">
    CopyRight © 2016. Powered By <a href="http://www.panda.tv/632096">http://www.panda.tv/632096</a>.
</div>
<!--底部 结束-->
</body>
<script>
    function _out() {
        layer.confirm('确认退出？', {
            btn: ['确认','取消'] //按钮
        }, function(){
            layer.msg('正在登出...',{icon:16,shade: 0.8,time:1500}, function(){
                //do something
                location.href = "loginout.php";
            });
        }, function(){
            layer.msg('已取消', {icon: 2, time: 1500}); //1.5秒关闭（如果不配置，默认是3秒）
        });
    }
</script>